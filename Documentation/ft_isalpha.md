# ft_isalpha
## Prototipo
```c
int ft_isalpha(int c);
```
### Descripción
ft_isalpha comprueba si el caracter es un caracter alfabetico del alfabeto latino en su vertiente inglesa
### Retorno
Si es un caracter alfabetico del alfabeto latino ingles devuelve 1, en caso contrario devuelve 0
## Funciones utilizadas
Ninguna