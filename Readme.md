# Libft Remake
## Introducción
Libft Remake es un remake de Libft diseñada para ganar tiempo con la original y para estar muy bien documentada (lo cual es imposible cumpliendo la norma), por motivos de practicidad se intentará que la documentación se pueda separar con un pequeño comando (creando una librería util para usarla en los proyectos)
## Funciones incluidas
Las funciones incluidas son las siguientes:
1. Pseudobooleanos
    1. <a href="#Documentaion/ft_isalpha.md">ft_isalpha</a>
